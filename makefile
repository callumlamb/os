# build environment
BOARD = bcm2835/
CPU = arm1176jzf-s
ARCH = armv6/
DATA_ARCH = arm
PREFIX = arm-none-eabi
BUILD = build/
SOURCE = source/
DATA = data/
LIBRARIES := libraries/

#platform dependant assembly files
OBJS        = $(patsubst $(SOURCE)$(ARCH)$(BOARD)%.s, $(BUILD)%.o, $(wildcard $(SOURCE)$(ARCH)$(BOARD)*.s))

#arch c files
OBJS        += $(patsubst $(SOURCE)$(ARCH)%.c, $(BUILD)%.o, $(wildcard $(SOURCE)$(ARCH)*.c))

#board c files
OBJS        += $(patsubst $(SOURCE)$(ARCH)$(BOARD)%.c, $(BUILD)%.o, $(wildcard $(SOURCE)$(ARCH)$(BOARD)*.c))

#c files
OBJS        += $(patsubst $(SOURCE)%.c, $(BUILD)%.o, $(wildcard $(SOURCE)*.c))

#Binary blobs
OBJS        += $(patsubst $(DATA)%.bin, $(BUILD)%.o, $(wildcard $(DATA)*.bin))

#Libraries
LIB_LIST   = $(patsubst $(LIBRARIES)%.a,$(LIBRARIES)%.a,$(wildcard $(LIBRARIES)*.a)) 
 
# Build flags
INCLUDES    := -I $(SOURCE)$(ARCH)$(BOARD)include -I $(SOURCE)include -I $(SOURCE)$(ARCH)include
BASEFLAGS   := -mcpu=$(CPU) -ffreestanding -O2 
WARNFLAGS   := -Wall -Wextra
LINKERFLAGS := -nostdlib -nostartfiles -nodefaultlibs
ASFLAGS     := $(INCLUDES) -D__ASSEMBLY__
CFLAGS      := $(INCLUDES) $(BASEFLAGS) $(WARNFLAGS) -std=gnu99

# build rules
all: kernel.img
 
include $(wildcard *.d)
 
kernel.elf: $(OBJS) link-arm-eabi.ld
	$(PREFIX)-ld $(OBJS) -L. $(LIBLIST) -Tlink-arm-eabi.ld $(LINKERFLAGS) -o $@
 
kernel.img: kernel.elf
	$(PREFIX)-objcopy kernel.elf -O binary kernel.img
 
clean:
	$(RM) -f $(OBJS) kernel.elf kernel.img
 
dist-clean: clean
	$(RM) -f *.d
	
 
# C independant
$(BUILD)%.o: $(SOURCE)%.c Makefile
	$(PREFIX)-gcc $(CFLAGS) -c $< -o $@
	
# C arch
$(BUILD)%.o: $(SOURCE)$(ARCH)%.c Makefile
	$(PREFIX)-gcc $(CFLAGS) -c $< -o $@
		
# C board
$(BUILD)%.o: $(SOURCE)$(ARCH)$(BOARD)%.c Makefile
	$(PREFIX)-gcc $(CFLAGS) -c $< -o $@
	
# Assembly
$(BUILD)%.o: $(SOURCE)$(ARCH)$(BOARD)%.s Makefile
	$(PREFIX)-gcc $(ASFLAGS) -c $< -o $@

# Data
$(BUILD)%.o: $(DATA)%.bin Makefile
	$(PREFIX)-objcopy -I binary -O $(PREFIX) -B $(DATA_ARCH) $< $@
