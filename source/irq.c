#include <stdint.h>
#include <irq.h>
#include <intc.h>
#include <exception.h>

//Array of IRQ handler pointers
void (*irq_handler[INTC_MAX_IRQ]) ();

/**
 * irq_handle_irq
 *
 * Find the correspoding IRQ handler and execute it
 */
void irq_handle() {
	//Get pending IRQ number
    int_fast16_t next_irq = intc_get_next_pending();
    while(next_irq != -1 && next_irq <= INTC_MAX_IRQ) {
        
        //Call the appropriate function handler
        irq_handler[next_irq]();
               
        //get next pending 
        next_irq = intc_get_next_pending();
                
    }
}

/**
 * irq_register_irq
 *
 * Register an IRQ
 */ 
void irq_register(uint_fast8_t irqn, void* function) {

	//Set the appropriate handler function pointer
	irq_handler[irqn] = function;
	
	//Unmask the IRQ line
	intc_unmask_irq(irqn);
	
}

/**
 * irq_register_irq
 *
 * Un-register an IRQ
 */
void irq_unregister(uint_fast8_t irqn) {
	
	//Mask the irq line
	intc_mask_irq(irqn);
	
}