#include <stdint.h>
#include <io.h>
#include <syscall.h>

/**
 * syscall_handle
 *
 * Entry point for the syscalls, this then routes to the correct handler.
 */
int syscall_handle(int function, int arg1) {
	switch(function) {
		case 0:			
			io_print(((const char *)arg1));
			return 0;
			break;
		default:
			return 1;
			break;
	}	
}