#ifndef IO_H
#define IO_H
 
void io_initialise();
void io_print(const char *str);

#endif