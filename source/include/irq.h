#ifndef IRQ_H
#define IRQ_H

void irq_register(uint_fast32_t irqn, void* function);
void irq_unregister(uint_fast32_t irqn);
void irq_handle();

#endif