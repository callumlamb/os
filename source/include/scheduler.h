#ifndef SCHEDULER_H
#define SCHEDULER_H

void scheduler_next();
void scheduler_add(void* entry);
void scheduler_remove();

#endif