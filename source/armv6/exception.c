#include <stdint.h>
#include <exception.h>
#include <io.h>
#include <irq.h>

//Exception types
#define EXCEPTION_RESET				0
#define EXCEPTION_UNDEFINED			0x1
#define EXCEPTION_SWI				0x2
#define EXCEPTION_PREFETCH_ABORT	0x3
#define EXCEPTION_DATA_ABORT		0x4
#define EXCEPTION_IRQ				0x6
#define EXCEPTION_FIQ				0x7

void __attribute__((interrupt("naked"))) _exception_swi();
void __attribute__((interrupt("naked"))) _exception_irq();
void _exception_set_vector(uint_fast8_t ndx, void* addr);

/**
 * exception_initialise
 *
 * Sets up the ARM ivt to stub routines that then redirect the exception to the appropriate handler in the kernel
 */
void exception_initialise() {

	//Set entry points for each kind of interrupt
	_exception_set_vector(EXCEPTION_SWI, _exception_swi);
	_exception_set_vector(EXCEPTION_IRQ, _exception_irq);
	
}

/**
 * exception_set_vector
 *
 * Set a vector to a certain pointer
 *
 * @param offset vector number to set
 * @param function pointer to function to call 
 */
void _exception_set_vector(uint_fast8_t ndx, void* addr) {
	uint32_t *v;
    v = (uint32_t*)0x0;
    v[ndx] = 0xEA000000 | (((uintptr_t)addr - 8 - (4 * ndx)) >> 2);
}

/**
 * exception_irq
 *
 * Redirect the IRQ to the IRQ handler
 */
void __attribute__((interrupt("naked"))) _exception_irq() {
	asm(
		"sub	lr, lr, #4; \
		srsdb   #31!; \
		cps     #31; \
		stmfd   sp!,{r0-r3,r12}; \
		and     r1, sp, #4; \
		sub     sp, sp, r1; \
		stmfd   sp!,{r1, lr}; \
		bl		irq_handle; \
		ldmfd   sp!,{r1,lr}; \
		add     sp, sp, r1; \
		ldmfd   sp!, {r0-r3, r12}; \
		rfeia   sp!"
	);
}

/**
 * exception_swi
 *
 * Redirect the exception and arguments to the syscall handler
 */
void __attribute__((interrupt("naked"))) _exception_swi() {
	asm(
		"stmfd  sp!,{r4, lr}; \
		 mrs    r4,spsr; \
		 stmfd  sp!,{r4}; \
		 bl		syscall_handle; \
		 ldmfd  sp!, {r4}; \
		 msr    spsr_cxsf, r4; \
		 ldmfd  sp!, {r4, pc}^;"
	);
}