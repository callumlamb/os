#include <stdint.h>

void _enable_mmu();
void _setup_descriptors();

void mmu_initialise() {
	//_setup_descriptors();
	//_enable_mmu();
}

/**
 * _setup_descriptors
 *
 * Setup first level and second level descriptors
 */
void _setup_descriptors() {

}

/**
 * _enable_mmu
 *
 * Turns on the MMU through the cp15 coprocessor.
 */
void _enable_mmu() {

	//Read c1 register from system control coprocessor (cp15)
	uint32_t c1; 
	asm("mrc p15, 0, %0, c1, c0, 0" : "=r" (c1) : : "cc");

	//Set bit M (0) to turn the MMU on
	c1 |= 1 << 0;
	
	//Write new value
	asm("mcr p15, 0, %0, c1, c0, 0" : : "r" (c1) : "cc");
	
}

