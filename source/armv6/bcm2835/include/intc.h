#ifndef INTC_H
#define INTC_H

//Maximum IRQ number
#define INTC_MAX_IRQ	72
 
int_fast16_t intc_get_next_pending();
void intc_mask_irq(uint32_t irqn);
void intc_unmask_irq(uint32_t irqn);
 
#endif