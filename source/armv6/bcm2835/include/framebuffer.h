#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

//framebuffer settings
#define FRAMEBUFFER_WIDTH 			1280
#define FRAMEBUFFER_HEIGHT 			1024

//Functions
uint16_t* framebuffer_initialise();

#endif