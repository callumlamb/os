#include <stdint.h>
#include <intc.h>

//Basic IRQs
#define	INTC_IRQ_TIMER				0
#define	INTC_IRQ_mailbox 			1
#define	INTC_IRQ_doorbell0			2
#define	INTC_IRQ_doorbell1			3
#define	INTC_IRQ_gpu0_halted		4
#define	INTC_IRQ_gpu1_halted		5
#define	INTC_IRQ_illegal1			6
#define	INTC_IRQ_illegal0			7

//Bank 1 IRQs
#define	INTC_IRQ_auxint				(29+8)

//Bank 2 IRQs
#define	INTC_IRQ_i2c_spi_slv_int	(43+8)
#define	INTC_IRQ_pwa0				(45+8)
#define	INTC_IRQ_pwa1				(46+8)
#define	INTC_IRQ_smi				(48+8)
#define	INTC_IRQ_gpio_int0			(49+8)
#define	INTC_IRQ_gpio_int1			(50+8)
#define	INTC_IRQ_gpio_int2			(51+8)
#define	INTC_IRQ_gpio_int3			(52+8)
#define	INTC_IRQ_i2c_int			(53+8)
#define	INTC_IRQ_spi_int			(54+8)
#define	INTC_IRQ_pcm_int			(55+8)
#define	INTC_IRQ_uart_int			(57+8)

//Controller IO
#define INTC_BASE					0x2000B000
#define INTC_PENDING_BASIC			((volatile uint32_t *)(INTC_BASE+0x200))
#define INTC_PENDING_IRQ1			((volatile uint32_t *)(INTC_BASE+0x204))
#define INTC_PENDING_IRQ2			((volatile uint32_t *)(INTC_BASE+0x208))
#define INTC_ENABLE_IRQ1			((volatile uint32_t *)(INTC_BASE+0x210))
#define INTC_ENABLE_IRQ2			((volatile uint32_t *)(INTC_BASE+0x214))
#define INTC_ENABLE_BASIC			((volatile uint32_t *)(INTC_BASE+0x218))

/**
 * intc_get_next_pending()
 *
 * Get current highest priority raised IRQ
 */
int_fast16_t intc_get_next_pending() {

	//Basic mask
	int_fast16_t shifts = 0;
	uint32_t mask = *INTC_PENDING_BASIC;
	while(mask) {
		if(mask & 1) return shifts;
		mask = mask >> 1;
		shifts++;
	}

	//IRQ1 mask
	shifts = 0;
	mask = *INTC_PENDING_IRQ1;	
	while(mask) {
		if(mask & 1) return shifts + 7;
		mask = mask >> 1;
		shifts++;
	}

	//IRQ2 mask
	shifts = 0;
	mask = *INTC_PENDING_IRQ2;
	while(mask) {
		if(mask & 1) return shifts + 39;
		mask = mask >> 1;
		shifts++;
	}
	
	//If no interrupts are pending then return minus 1
	return -1;
	
 }

/**
 * intc_mask_irq
 *
 * Mask an IRQ in a bank
 *
 * @param irqn IRQ to mask
 */
void intc_mask_irq(uint32_t irqn) {
	if(irqn <= 7) {
		*INTC_ENABLE_BASIC &= 0xFFFFFFFF - (1 << irqn);
	} else if(irqn <= 39) {
		*INTC_ENABLE_IRQ1 &= 0xFFFFFFFF - (1 << (irqn-8));
	} else if(irqn <= 71) {
		*INTC_ENABLE_IRQ2 &= 0xFFFFFFFF - (1 << (irqn-8-32));
	}
}

/**
 * intc_unmask_irq
 *
 * Unmask an irq in a bank
 *
 * @param irqn IRQ to unmask
 */
void intc_unmask_irq(uint32_t irqn) {
	if(irqn <= 7) {
		*INTC_ENABLE_BASIC |= (1 << irqn);
	} else if(irqn <= 39) {
		*INTC_ENABLE_IRQ1 |= (1 << (irqn-8));
	} else if(irqn <= 71) {
		*INTC_ENABLE_IRQ2 |= (1 << (irqn-8-32));
	}
}