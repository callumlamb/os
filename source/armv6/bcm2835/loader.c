#include <stdint.h>
#include <main.h>

//C1 register flags
#define C1_MMU					(1 << 0)
#define C1_DATA_CACHE			(1 << 2)
#define C1_INSTRUCTION_CACHE 	(1 << 12)
#define C1_BRANCH_PREDICTION	(1 << 11)

#define NUM_PAGE_TABLE_ENTRIES 	4096
#define CACHE_DISABLED    		0x12
#define SDRAM_START       		0x0
#define SDRAM_END         		0x20000000
#define CACHE_WRITEBACK   		0x1e

void __attribute__((section (".text.loader"))) loader() {

	//Map virt address 0x80000000 to (0x80000000 + kernel size) to 0x8000 to (0x8000 + kernel size)
	//relocate and map IVT to higher location so it can modified in the kernel
	//Map ports to higher location
	

	//Page tables
	static uint32_t __attribute__((aligned(16384))) page_table[NUM_PAGE_TABLE_ENTRIES];

    //Set up an identity-mapping for all 4GB, rw for everyone
    for (uint32_t i = 0; i < NUM_PAGE_TABLE_ENTRIES; i++) page_table[i] = i << 20 | (3 << 10) | CACHE_DISABLED;
    
	//Then, enable cacheable and bufferable for RAM only
    for (uint32_t i = SDRAM_START >> 20; i < SDRAM_END >> 20; i++) page_table[i] = i << 20 | (3 << 10) | CACHE_WRITEBACK;

    //Copy the page table address to cp15
    asm volatile("mcr p15, 0, %0, c2, c0, 0" : : "r" (page_table) : "memory");
	
    //Set the access control to all-supervisor
    asm volatile("mcr p15, 0, %0, c3, c0, 0" : : "r" (~0));

	//Enable MMU
	uint32_t c1; 
	asm("mrc p15, 0, %0, c1, c0, 0" : "=r" (c1) : : "cc");
	c1 |= C1_MMU;
	asm("mcr p15, 0, %0, c1, c0, 0" : : "r" (c1) : "cc");
	
	//Enable data/instruction cache and branch prediction on the CPU
	asm("mrc p15, 0, %0, c1, c0, 0" : "=r" (c1) : : "cc");
	c1 |= C1_BRANCH_PREDICTION + C1_DATA_CACHE + C1_INSTRUCTION_CACHE;
	asm("mcr p15, 0, %0, c1, c0, 0" : : "r" (c1) : "cc");
	
	//Call entry point for kernel
	main();
	
}

