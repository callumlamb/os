.section ".text.boot"
.globl _start

// Preserve the following for the kernel 
// r15 -> should begin execution at 0x8000.
// r0 -> 0x00000000
// r1 -> 0x00000C42
// r2 -> 0x00000100 - start of ATAGS
start:

	//Setup mode stacks
	msr		cpsr, #0b11010001
	ldr		sp, =_fiq_stack	
	msr		cpsr, #0b11010010
	ldr		sp, =_irq_stack	
	msr		cpsr, #0b11011011
	ldr		sp, =_undef_stack			
	msr		cpsr, #0b11010111
	ldr		sp, =_abort_stack	
	msr		cpsr, #0b11010011
	ldr		sp, =_svc_stack
	
	//Clear BSS
	ldr   	r3, =_bss_end
	ldr   	r4, =_bss_start
	cmp   	r4, r3
	bcs   	2f
	sub   	r3, r4, #1
	ldr   	r5, =_bss_end
	sub		r5, r5, #1
	mov	   	r4, #0
	1:		
			strb   	r4, [r3, #1]!
			cmp    	r3, r5
			bne		1b
	2:
	
	// Call loader
	ldr	r3, =loader
	blx	r3
 
	//Halt
	halt:
	wfe
	b	halt
