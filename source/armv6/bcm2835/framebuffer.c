#include <stdint.h>
#include <framebuffer.h>

//Mailbox constants
#define MAILBOX_WRITE_READY 	0x80000000
#define MAILBOX_READ_READY  	0x40000000
#define MAILBOX_BASE   			0x2000B880
#define MAILBOX_READ   			((volatile uint32_t *) (MAILBOX_BASE))
#define MAILBOX_STATUS 			((volatile uint32_t *) (MAILBOX_BASE + 0x18))
#define MAILBOX_WRITE  			((volatile uint32_t *) (MAILBOX_BASE + 0x20))

//Mailbox flags
#define MAILBOX_WRITE_CLEAR_CACHE 	0x40000000

void _framebuffer_write(uint32_t value);
uint32_t _framebuffer_read();

//Types
typedef struct {
	uint32_t  width;
	uint32_t  height;
	uint32_t  vWidth;
	uint32_t  vHeight;
	uint32_t  pitch;
	uint32_t  bitDepth;
	uint32_t  x;
	uint32_t  y;
	uint16_t* pointer;
	uint32_t  size;
} __attribute__((aligned(16))) FrameBufferDescription;

//Framebuffer structure
volatile FrameBufferDescription frameBufferInfo = {FRAMEBUFFER_WIDTH,FRAMEBUFFER_HEIGHT,FRAMEBUFFER_WIDTH,FRAMEBUFFER_HEIGHT,0,16,0,0,0,0};

/**
 * framebuffer_write
 *
 * Write to the framebuffer.
 *
 * @param value the value to send, lowest 4 bits must be empty
 */
void _framebuffer_write(uint32_t value) {

	//Wait until mailbox status indicates it's ready to send mail
	while((*MAILBOX_STATUS & MAILBOX_WRITE_READY) != 0);	
	
	//Send mail with GPU channel in the lowest 4 bits
	*MAILBOX_WRITE = value + 1;	
}

/**
 * framebuffer_read
 *
 * Read back from the framebuffer.
 *
 * @return The mail from the GPU
 */
uint32_t _framebuffer_read() {

	//Wait until status indicates the MAILBOX has mail ready
	while((*MAILBOX_STATUS & MAILBOX_READ_READY) != 0);	

	//Keep getting mail until the mail for channel 1 is recieved
	uint32_t mail;
	do {
		mail = *MAILBOX_READ;
	} while((mail & 0xF) != 1);	

	//Return the mail (with the channel masked out)
	return (mail & 0xFFFFFFF0);	
}

/**
 * framebuffer_initialise        
 *
 * Setup the framebuffer settings and framebuffer
 */
uint16_t* framebuffer_initialise() {	
		
	//Setup framebuffer
	_framebuffer_write((uint32_t)&frameBufferInfo + MAILBOX_WRITE_CLEAR_CACHE);
	uint32_t result = _framebuffer_read();
		
	//If the result is non-zero then hang
	if(result != 0) while(1);
	
	//Wait until pointer becomes available
	while(frameBufferInfo.pointer == 0); 
		
	//Set pointer variable for general use (non-volatile since it won't change now, makes optimization work better)
	return frameBufferInfo.pointer;
	
}


