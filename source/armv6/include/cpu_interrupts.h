#ifndef CPU_INTERRUPTS_H
#define CPU_INTERRUPTS_H

/**
 * CPU_INTERRUPTS_ENABLE
 *
 * Clears the interrupt mask flag is cpsr
 */
static inline void CPU_INTERRUPTS_ENABLE() {
	asm("cpsie i");	
}

/**
 * CPU_INTERRUPTS_DISABLE
 *
 * Sets the interrupt mask flag is cpsr
 */
static inline void CPU_INTERRUPTS_DISABLE() {
	asm("cpsid i");
}

#endif