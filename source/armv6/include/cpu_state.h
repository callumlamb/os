#include <stdint.h>

/**
 * Pcb
 *
 * Structure holding the current processor state
 */
typedef struct {
	uint32_t r14;
	uint32_t r13;
	uint32_t r12;
	uint32_t r11;
	uint32_t r10;
	uint32_t r9;
	uint32_t r8;
	uint32_t r7;
	uint32_t r6;
	uint32_t r5;
	uint32_t r4;
	uint32_t r3;
	uint32_t r2;
	uint32_t r1;
	uint32_t r0;
	uint32_t lr;
	uint32_t spsr;
} Pcb;

/**
 * SAVE_STATE
 *
 * Save the current CPU state in the requested Pcb
 */
//static inline void SAVE_STATE(Pcb* state) {
	//TODO
//}

/**
 * LOAD_STATE
 *
 * Load the CPU state in the Pcb structure onto the CPU
 */
//static inline void LOAD_STATE(Pcb* state) {
	//TODO
//}