#ifndef CPU_SYSCALL_H
#define CPU_SYSCALL_H

/**
 * CPU_SYSCALL
 *
 * Generates a system call instruction
 */
static inline uint32_t CPU_SYSCALL(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3) {
	uint32_t value;
	asm volatile(
	   "mov r0, %1;\
		mov r1, %2;\
		mov r2, %3;\
		mov r3, %4;\
		swi #0; \
		mov %0, r0"
		: "=r" (value)
		: "r" (r0), "r" (r1), "r" (r2), "r" (r3)
		: "r0", "r1", "r2", "r3", "ip", "lr", "memory", "cc"
	);
	return value;
}

#endif