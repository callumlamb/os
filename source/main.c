#include <stdint.h>
#include <irq.h>
#include <timer.h>
#include <clock.h>
#include <exception.h>
#include <scheduler.h>
#include <io.h>
#include <cpu_interrupts.h>
#include <mmu.h>
#include <main.h>

//TODO
//Well FUCK. I need to write an elf loader img that loads the kernel in elf format
//	if i use a the ramfs in config.txt to load my kernel.elf at a memory address then all i have to do is process the file in memory
//MMU
//	Alter MMU mapping so it maps kernel to it's final location along with IO ports with proper properties, use BCM peripherals
//		Use the split feature (separate page tables for kernel and user) tables for kernel and user so only one needs to be switched.
//	Make sure all available caching and optimizations are enabled
//  	Make sure branch prediction is enabled in c1
//		Make sure level 1 data and instruction caches are enabled in c1
//Context switch http://infocenter.arm.com/help/topic/com.arm.doc.dui0471i/Cjaebbeh.html
//intterrupts/exceptions
//  Do other exception entries
//Evaluate HAL api integer sizes .etc are they abstract and general enough, do they use fast types where possible?
//Entry routine for the architecture so standardised arguments can be passed to kernel_main. Mimick the multiboot data structure.
//irq table in irq.c should be malloced to size of highest irq when kmalloc is available
	//Same with all the context tables .etc


void a() {
	while(1) {
		io_print("A");
	}
}

void b() {
	while(1) {
		io_print("B");
	}
}
 
/**
 * main
 *
 * Entry point
 */
void main() {
	
	//Setup memory management
	mmu_initialise();
	
	//Setup output api
	io_initialise();
	io_print("System started.\n");
	
	//Setup interrupts
	exception_initialise();
	
	//Load 2 processes
	scheduler_add(a);
	scheduler_add(b);
	
	//Setup context switcher
	timer_initialise(0xFFFFF);
	irq_register(TIMER_IRQ, scheduler_next);
	CPU_INTERRUPTS_ENABLE();
		
}

