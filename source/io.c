#include <stdint.h>
#include <framebuffer.h>
#include <io.h>

void _io_update();
void _io_scroll();
void _io_draw_character(uint8_t character, uint_fast32_t start_x, uint_fast32_t start_y);

//Font properties
#define FONT_WIDTH   	 		8
#define FONT_HEIGHT 			16

//Terminal properties
#define TERMINAL_WIDTH  		(FRAMEBUFFER_WIDTH/FONT_WIDTH)
#define TERMINAL_HEIGHT 		(FRAMEBUFFER_HEIGHT/FONT_HEIGHT)

//font.bin binary pointers
extern uint8_t _binary_data_font_bin_start;
extern uint8_t _binary_data_font_bin_end;
extern uint32_t _binary_data_font_bin_size;

//Framebuffer
uint16_t* framebuffer;

//Terminal buffer
uint8_t terminal_screen[TERMINAL_WIDTH * TERMINAL_HEIGHT];
uint8_t terminal_buffer[TERMINAL_WIDTH * TERMINAL_HEIGHT];

//Locations
uint_fast32_t terminal_start = 0;
uint_fast32_t terminal_stop = 0;
uint_fast32_t endOfScrollPoint = 0;


/**
 * io_initialise
 *
 * Initialise the framebuffer and get a pointer
 */
void io_initialise() {
	framebuffer = framebuffer_initialise();
}

/**
 * io_update
 *
 * Update the screen to be the buffer and render all changes on the screen.
 */
void _io_update() {

	//Loop through each character from the start of changes to the end
	for(uint_fast32_t i = terminal_start; i < terminal_stop; i++) {		
	
		//If the buffer is different from the screen draw the change and update the screen buffer
		if(terminal_screen[i] != terminal_buffer[i]) {
			_io_draw_character(terminal_buffer[i], i % TERMINAL_WIDTH, i / TERMINAL_WIDTH);
			terminal_screen[i] = terminal_buffer[i];
		}
	}
	
	//Set the start of the changes back to the cursor
	terminal_start = terminal_stop;
}

/**
 * io_scroll
 *
 * Scroll the text buffer one row up
 */
void _io_scroll() {

	//Set end of scroll point for clearing rest of bottom line later
	endOfScrollPoint = terminal_stop;
	
	//Counter used for loops.
	uint_fast32_t i = 0;
	
	//Move all character TERMINAL_WIDTH backwards
	for(; i < (TERMINAL_WIDTH * TERMINAL_HEIGHT) - TERMINAL_WIDTH; i++) {
		terminal_buffer[i] = terminal_buffer[i+TERMINAL_WIDTH];
	}
	
	//Clear last row
	for(; i < terminal_stop; i++) {
		terminal_buffer[i] = 0;
	}
	
	//Set start to beginning to next update does the whole screen
	terminal_start = 0;
	
	//Set end of change to beginning of the last line
	terminal_stop = (TERMINAL_WIDTH*TERMINAL_HEIGHT) - TERMINAL_WIDTH;		
}

/**
 * io_print
 *
 * Print text on the screen
 *
 * @param str The string to print.
 */
void io_print(const char *str) {

	//Reset end of scroll point
	endOfScrollPoint = 0;
	
	//Update buffer
	while(*str) {	
	
		//Get next character
		uint8_t character = *str++;
		
		//Handle new lines
		if(character == '\n') {
		
			//Move end of change to next line
			terminal_stop += TERMINAL_WIDTH - (terminal_stop % TERMINAL_WIDTH);
			
		}
		
		//Handle normal characters
		else { 
		
			//Scroll is cursor is beyond the end of the buffer	
			if(terminal_stop >= (TERMINAL_WIDTH * TERMINAL_HEIGHT)) _io_scroll();

			//Add character to queue to be rendered on next update
			terminal_buffer[terminal_stop] = character;
			terminal_stop++;			

		}		
	}

	//Perform a full screen update where a scroll has happened.
	if(endOfScrollPoint) {
		
		//Do an update to the end of the last line just before the scroll happened
		terminal_stop = endOfScrollPoint;
		_io_update();
		
		//Set start of changes and end to start of the last line
		terminal_start = (TERMINAL_WIDTH*TERMINAL_HEIGHT) - TERMINAL_WIDTH;
		terminal_stop = (TERMINAL_WIDTH*TERMINAL_HEIGHT) - TERMINAL_WIDTH;
	}
	
	//Perform an update where a scroll has not happened
	else _io_update();
	
}

/**
 * _io_draw_character
 *
 * Draws a character at the specified x,y pixel coordinate
 *
 * @param character Ascii character to draw
 * @param start_x x coordinate pixel to draw at
 * @param start_y y coordinate pixel to draw at
 */
void _io_draw_character(uint8_t character, uint_fast32_t start_x, uint_fast32_t start_y) {

	//Don't bother with non-printable characters
	if(character < 33 || character > 126) character = 0;

	//get address of character in binary blob
	uint8_t* charAddress = (uint8_t*) &_binary_data_font_bin_start + (character * FONT_HEIGHT); 
	
	//loop through each column of the character
	for(uint_fast32_t row = 0; row < FONT_HEIGHT; row++) { 
	
		//Get address of row of bits in the font file
		uint8_t *bits = charAddress + row;
	
		//Get offset into framebuffer where the first row of bits will go
		uint_fast16_t y = ((row + (start_y * FONT_HEIGHT)) * FRAMEBUFFER_WIDTH);
		
		//Loop through each row of the character
		for(uint_fast8_t bit = 0; bit < FONT_WIDTH; bit++) { 
		
			//Calculate offset from y into framebuffer this bit will go
			uint_fast16_t x = (bit + (start_x*FONT_WIDTH));
			
			//Either set or unset the pixel depending on the bit
			uint16_t colour = 0;
			if(*bits >> bit & 0x1) colour = 0x7c0;
			
			//Write the pixel
			*((volatile uint16_t *) framebuffer + x + y) = colour;		
		}	
	}		
}